package sets
//represent sets of integers as map[int]bool
//faster versions exist, but this one is simple 

//represents the set
type IntSet struct {
	set  map[int]bool
	size int
}

//create a new string set
func NewIntSet() *IntSet {
	return &IntSet{make(map[int]bool), 0}
}

//Add a string to the set
func (self *IntSet) Add(item int) bool {
	if !self.set[item] {
		self.set[item] = true
		self.size++
		return true
	}
	return false
}

//Remove a string from the set
func (self *IntSet) Remove(item int) bool {
	if self.set[item] {
		delete(self.set, item)
		self.size--
		return true
	}
	return false
}

//Get the set contents as an array of strings
func (self *IntSet) Contents() []int {
	ret := make([]int, len(self.set))
	i := 0
	for key, _ := range self.set {
		ret[i] = key
		i++
	}
	return ret
}
func (self *IntSet) Size() int {
	return self.size
}
func (self *IntSet) Contains(item int) bool {
	return self.set[item]
}
func (a *IntSet) Union(b *IntSet) {
	for k, _ := range b.set {
		a.Add(k)
	}
}
func (a *IntSet) Intersection(b *IntSet) {
	for k, _ := range a.set {
		if !b.set[k] {
			a.Remove(k)
		}
	}
}
func (a *IntSet) Difference(b *IntSet) {
	for k, _ := range a.set {
		if b.set[k] {
			a.Remove(k)
		}
	}
}
func (a *IntSet) Jaquard(b *IntSet) float64 {
	i := NewIntSet()
	u := NewIntSet()
	i.Union(a)
	i.Intersection(b)
	u.Union(a)
	u.Union(b)
	j := float64(i.Size()) / float64(u.Size())
	return j

}

func (a *IntSet) AddArray(items []int) {
	for _, v := range items {
		a.Add(v)
	}
}
func AddIntArray(a *IntSet, items []int) {
	for _, v := range items {
		a.Add(v)
	}
}
func AddInt(a *IntSet, item int) {
	a.Add(item)
}
