package sets

//interfaces for set types
//general format is interface definition and some package functions and subroutines to work with the interface

//interface for a set of strings
type StringsSet interface {
	Add(item string)
	Remove(item string)
	Contents() []string
	Size() int
	Contains(item string) bool
	Union(a StringsSet)
	Intersection(a StringsSet)
	Difference(a StringsSet)
}

func Union(a ...StringsSet) StringsSet {
	c := NewStringSet()
	ch := make(chan []string, len(a))
	done := make(chan int, len(a))
	final := make(chan struct{}) //nil value only care if it closes
	go func() {
		for strs := range ch {
			c.AddArray(strs)
		}
		close(final) //makes ready to recieve with no values sent
	}()
	go func() {
		for _, set := range a {
			//go func(a StringsSet) {
			ch <- set.Contents()
			done <- 1
			//		}(set)
		}
	}()
	//make sure we have all of the set contents processed
	for d := len(a); d > 0; d -= <-done {
	}
	close(done)
	close(ch)
	<-final //close channel is ready to be reciever
	return c

}
func Intersection(a ...StringsSet) StringsSet {
	c := Union(a[0])
	for _, set := range a[1:] {
		for _, item := range set.Contents() {
			if !c.Contains(item) {
				c.Remove(item)
			}
		}
	}

	return c
}
func Difference(a, b StringsSet) StringsSet {
	c := Union(a)
	for _, k := range a.Contents() {
		if b.Contains(k) {
			c.Remove(k)
		}
	}
	return c
}

//interface for a set of integers
type IntsSet interface {
	Add(item int)
	Remove(item int)
	Contents() []int
	Size() int
	Contains(item int) bool
	Union(a IntsSet)
	Intersection(a IntsSet)
	Difference(a IntsSet)
}

//interface for a tiny set of not more than 64 bits
type Switch uint
type Bits uint64
type BitsSet interface {
	//CheckSwitch(s Switch)
	SetSwitch(s Switch)
	ClearSwitch(s Switch)
	SetBits(b Bits)
	ClearBits(b Bits)
	Contents() uint64
	Binary() string
	Size() int
	IsSet(s Switch) bool
	HasBits(b Bits) bool
	Union(a BitsSet)
	Intersection(a BitsSet)
	Difference(a BitsSet)
}
