package sets

import (
    "fmt"
    "strconv"
)
const (
	switchmin Switch = 0
	switchmax Switch = 64
)

//represents the set
type BitSet struct {
	set  Bits
	size int // do we need this at all ?
}

//create a new bit set, longhand for &BitSet{0,0}
func NewBitSet() *BitSet {
	return new(BitSet)
}

//ensure switch is within range
func checkSwitch(s Switch) bool {
	if s <= switchmax && s >= switchmin {
		return true
	}
	return false
}

//Set a switch in the set
func (self *BitSet) SetSwitch(s Switch) {
	if checkSwitch(s) {
		self.set |= 1 << s
	}
}
func (self *BitSet) SetSwitches(s ...Switch) {
	for v := range s {
		self.SetSwitch(Switch(v))
	}
}
func (self *BitSet) SetSwitchArray(s []Switch) {
	for _, v := range s {
		self.SetSwitch(v)
	}
}
func (self *BitSet) SetBits(bits Bits) {
	self.set |= bits
}
func (self *BitSet) ClearSwitch(s Switch) {
	if checkSwitch(s) {
		self.set &^= 1 << s
	}
}
func (self *BitSet) ClearBits(bits Bits) {
	self.set &^= bits
}

//Get the set contents as an array of strings
func (self *BitSet) Contents() uint64 {
	return uint64(self.set)
}
func (self *BitSet) Binary() string {
    return strconv.FormatUint(self.Contents(),2)
}
func (self *BitSet) Size() int {
	z := Bits(0)
	s := self.set
	test := z ^ s
	d := 0
	for test > Bits(0) {
		d++
		test &= test - 1
	}
	return d
}
func (self *BitSet) IsSet(s Switch) bool {
	if checkSwitch(s) && self.set&1<<s == self.set {
		return true
	}
	return false
}
func (self *BitSet) HasBits(b Bits) bool {
	if self.set&b == self.set {
		return true
	}
	return false
}
func (a *BitSet) Union(b BitsSet) {
	a.set |=Bits( b.Contents())
}
func (a *BitSet) Intersection(b BitsSet) {
	a.set &= Bits(b.Contents())
}
func (a *BitSet) Difference(b BitsSet) {
	a.set &^= Bits(b.Contents())
}
func (a *BitSet) Jaquard(b BitsSet) float64 {
	i := NewBitSet()
	u := NewBitSet()
	i.Union(a)
	fmt.Println("i:", i)
	i.Intersection(b)
	fmt.Println("i:", i)
	u.Union(a)
	fmt.Println("u:", u)
	u.Union(b)
	fmt.Println("u:", u)
	j := float64(i.Size()) / float64(u.Size())
	return j

}
