package sets

import (
	"math"
	"strconv"
	"testing"
)

var set1 *StringSet = &StringSet{map[string]bool{"A": true, "B": true, "C": true}, 3}
var set2 *StringSet = &StringSet{set: map[string]bool{"B": true, "C": true, "D": true}, size: 3}
var set3 *StringSet = &StringSet{map[string]bool{"A": true, "E": true, "G": true}, 3}

//also the union expect cases
var AddExpect1 *StringSet = &StringSet{map[string]bool{"A": true, "B": true, "C": true}, 3}
var AddExpect2 *StringSet = &StringSet{map[string]bool{"A": true, "B": true, "C": true, "D": true}, 4}
var AddExpect3 *StringSet = &StringSet{map[string]bool{"A": true, "B": true, "C": true, "D": true, "E": true, "G": true}, 5}

var IntersectionExpect1 *StringSet = &StringSet{map[string]bool{"B": true, "C": true}, 6}
var IntersectionExpect2 *StringSet = NewStringSet()

func TestAdd(tst *testing.T) {
	testSet := NewStringSet()
	tst.Log("testing Add")
	for _, k := range set1.Contents() {
		testSet.Add(k)
	}
	tst.Log(testSet.Contents())
	if len(testSet.Contents()) == 3 {
		tst.Log("correct number")
	}
	for _, k := range testSet.Contents() {
		switch AddExpect1.Contains(k) {
		case true:
			{
				tst.Log("expected value :", k)
			}
		case false:
			{
				tst.Error("unexpected value: ", k)
			}
		}
	}
	for _, k := range set2.Contents() {
		AddArray(testSet, []string{k})
	}
	if len(testSet.Contents()) == 4 && testSet.Size() == 4 {
		tst.Log("correct number of items in set")
	} else {
		tst.Error("wrong number of items in set", len(testSet.Contents()))
	}
	for _, k := range testSet.Contents() {
		switch AddExpect2.Contains(k) {
		case true:
			{
				tst.Log("expected value :", k)
			}
		case false:
			{
				tst.Error("unexpected value: ", k)
			}
		}
	}
	for _, k := range set3.Contents() {
		testSet.Add(k)
	}
	if len(testSet.Contents()) == 6 {
		tst.Log("correct number of items in set")
	} else {
		tst.Error("wrong number of items in set", len(testSet.Contents()))
	}
	for _, k := range testSet.Contents() {
		switch AddExpect3.Contains(k) {
		case true:
			{
				tst.Log("expected value :", k)
			}
		case false:
			{
				tst.Error("unexpected value: ", k)
			}
		}
	}

}

func TestUnion(tst *testing.T) {
	tst.Log("Testing Union")
	testSet := NewStringSet()
	AddArray(testSet, set1.Contents())
	tst.Log(testSet)
	for _, k := range testSet.Contents() {
		switch AddExpect1.Contains(k) {
		case true:
			{
				tst.Log("expected value :", k)
			}
		case false:
			{
				tst.Error("unexpected value: ", k)
			}
		}
	}
	testSet.Union(set2)
	tst.Log(testSet)
	for _, k := range testSet.Contents() {
		switch AddExpect2.Contains(k) {
		case true:
			{
				tst.Log("expected value :", k)
			}
		case false:
			{
				tst.Error("unexpected value: ", k)
			}
		}
	}
	testSet.Union(set3)
	tst.Log(testSet)
	for _, k := range testSet.Contents() {
		switch AddExpect3.Contains(k) {
		case true:
			{
				tst.Log("expected value :", k)
			}
		case false:
			{
				tst.Error("unexpected value: ", k)
			}
		}
	}
}

func TestIntersection(tst *testing.T) {
	tst.Log("testing Intersection")
	testSet := NewStringSet()
	testSet.Union(set1)
	testSet.Intersection(set2)
	if len(testSet.Contents()) == 2 {
		tst.Log("correct length after intersection ")
	} else {
		tst.Error("wrong length after intersection")
	}
	for _, k := range testSet.Contents() {
		switch IntersectionExpect1.Contains(k) {
		case true:
			tst.Log("expected value:", k)
		case false:
			tst.Error("unexpected value:", k)
		}
	}
	testSet.Intersection(set3)
	if len(testSet.Contents()) == 0 {
		tst.Log("no members left in set after test Intersection, okay")
	} else {
		tst.Log("unexpected members left in set after test intersection")
	}
}

func TestDifference(tst *testing.T) {
	tst.Log("testing set Difference")
	testSet := NewStringSet()
	testSet.Union(set2)
	testSet.Difference(set1)
	if len(testSet.Contents()) == 1 && testSet.Contains("D") {
		tst.Log("passed simple set difference test ", testSet)
	} else {
		tst.Log("failed to pass simple set difference test")
		tst.Error("testSet:", testSet)
	}
}
func TestJaquard(tst *testing.T) {
	answer := float64(3) / float64(5)
	tst.Log("testing Jaquard similarity of sets")
	a := NewStringSet()
	b := NewStringSet()
	c := NewIntSet()
	d := NewIntSet()
	e := NewBitSet()
	f := NewBitSet()
	e.SetBits(1872134923458712341)
	e.SetSwitches(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
	e.SetSwitchArray([]Switch{11, 12, 13, 14, 15, 16, 17, 18, 19, 10})
	f.SetSwitch(1)
	f.SetSwitch(2)
	f.SetSwitch(4)
	f.SetSwitch(8)
	f.SetSwitch(3)
	f.SetSwitch(32)
	f.SetSwitch(12)
	f.SetSwitch(9)
	for i := 0; i < 64; i++ {
		f.SetSwitch(Switch(i))
		tst.Log("f:%v\n", f.Binary())
	}
	r := e.Jaquard(f)
	tst.Log("e:", e.Contents())
	tst.Log("e:", strconv.FormatUint(e.Contents(), 2))
	tst.Log("e.Size()", e.Size())
	tst.Log("f:", f.Contents())
	tst.Log("f:", strconv.FormatUint(f.Contents(), 2))
	tst.Log("f size:", f.Size())
	tst.Log("r:", r)
	//"AATATACAATATAT"
	//"AATATATAATATAT"
	a.AddArray([]string{"AA", "AT", "TA", "AC", "CA"})
	b.AddArray([]string{"AA", "AT", "TA"})
	c.AddArray([]int{1, 2, 3, 5, 7, 11, 13})
	d.AddArray([]int{2, 3, 4, 5, 6, 7, 8})
	j := a.Jaquard(b)
	ji := c.Jaquard(d)
	a.AddArray([]string{"AAT", "ATA", "TAT", "TAC", "ACA", "CAA"})
	b.AddArray([]string{"AAT", "ATA", "TAT", "TAA"})
	j2 := a.Jaquard(b)
	d1 := math.Log2(j)
	d2 := math.Log2(j2)
	d2 = math.Log2(0.99787)
	tst.Log("distance1", -d1)
	tst.Log("distance2", -d2)
	tst.Log("a", a, "b", b)
	tst.Log("jaquard similarity:", j)
	tst.Log("jaquard3mers similarity:", j2)
	if j == answer {
		tst.Log("passed jaquard test")
	} else {
		tst.Error("failed jaquard test")
	}
	tst.Log("int set jaquard for", c, " and ", d)
	tst.Log("ji:", ji)

}
func TestSetsUnion(t *testing.T) {
	a := NewStringSet()
	b := NewStringSet()
	c := NewStringSet()
	d := NewStringSet()
	j := "AAATATAACACACCCAAGAGAGGAGAGHGHAGAHTTTACATACCTAHCACCCGCHHCHACCHACCDCDATUHI"
	for i := 1; i < -2+len(j); i++ {
		d.Add(string(j[i : i+2]))
	}
	a.AddArray([]string{"AA", "AT", "AC", "AG"})
	b.AddArray([]string{"CA", "CT", "CC", "CG"})
	c.AddArray([]string{"GA", "GT", "GC", "GG"})
	d.AddArray([]string{"TT", "TC", "TA", "TG"})
	u := Union(a, b, c, d)
	for i := 0; i <= 1000000; i++ {
		u = Union(u, a, b, c, d)
	}
	i := Intersection(b, u)
	t.Log("TestSetsUnion:", u)
	t.Log("i:", i)
}
