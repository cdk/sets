package sets

//representation of stringset in go matching interface in sets.go

//represents the set
type StringSet struct {
	set  map[string]bool
	size int
}

//create a new string set
func NewStringSet() *StringSet {
	return &StringSet{make(map[string]bool), 0}
}

//Add a string to the set
func (self *StringSet) Add(item string) {
	if !self.set[item] {
		self.set[item] = true
		self.size++
	}
}

//Remove a string from the set
func (self *StringSet) Remove(item string) {
	if self.set[item] {
		delete(self.set, item)
		self.size--
	}
}

//Get the set contents as an array of strings
func (self *StringSet) Contents() []string {
	ret := make([]string, len(self.set))
	i := 0
	for key, _ := range self.set {
		ret[i] = key
		i++
	}
	return ret
}
func (self *StringSet) Size() int {
	return self.size
}
func (self *StringSet) Contains(item string) bool {
	return self.set[item]
}
func (a *StringSet) Union(b StringsSet) {
	for _, k := range b.Contents() {
		a.Add(k)
	}
}
func (a *StringSet) Intersection(b StringsSet) {
	for k, _ := range a.set {
		if !b.Contains(k) {
			a.Remove(k)
		}
	}
}
func (a *StringSet) Difference(b StringsSet) {
	for k, _ := range a.set {
		if b.Contains(k) {
			a.Remove(k)
		}
	}
}
func (a *StringSet) Jaquard(b *StringSet) float64 {
	i := NewStringSet()
	u := NewStringSet()
	i.Union(a)
	i.Intersection(b)
	u.Union(a)
	u.Union(b)
	j := float64(i.Size()) / float64(u.Size())
	return j

}

func Jaquard(a, b StringsSet) float64 {
	i := NewStringSet()
	u := NewStringSet()
	i.Union(a)
	i.Intersection(b)
	u.Union(a)
	u.Union(b)
	j := float64(i.Size()) / float64(u.Size())
	return j
}

func (a *StringSet) AddArray(items []string) {
	for _, v := range items {
		a.Add(v)
	}
}
func AddArray(a *StringSet, items []string) {
	for _, v := range items {
		a.Add(v)
	}
}
func Add(a *StringSet, item string) {
	a.Add(item)
}
